import Swal from 'sweetalert2'
import { Component, OnInit } from '@angular/core';
import { MenuService } from '../services/menu.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ApiService } from '../services/api.service';

declare var $:any;

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  userForm: FormGroup;
  passwordForm: FormGroup;
  paises: any;
  error: boolean = false;

  tab: number = 1;

  constructor(public menu: MenuService, public formBuilder: FormBuilder, private api: ApiService) { }

  ngOnInit(): void {
    $('#loading').show();

    this.userForm = this.createMyUserForm();
    this.passwordForm = this.createMyPasswordForm();
    this.llenarCampos();
    this.getCountry();
  }

  getCountry(){
    this.api.getCountry().subscribe(result => {
      let resultado = result['data'];
      
      if (resultado.response == 1) {
        this.paises = resultado.data;
      }

      $('#loading').hide();
    });
  }

  private createMyUserForm(){
    return this.formBuilder.group({
      NOMBRE: ['', [Validators.required]],
      TELEFONO: ['', [Validators.required, Validators.maxLength(10)]],
      EMAIL: ['', [Validators.required, Validators.pattern('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$')]],
      CIUDAD: ['', [Validators.required]],
      PAIS: ['', [Validators.required]]
    });
  }

  private createMyPasswordForm(){
    return this.formBuilder.group({
      PASSWORD: ['', [Validators.required, Validators.minLength(6)]],
      NEW_PASSWORD: ['', [Validators.required, Validators.minLength(6)]],
      EMAIL: ['', [Validators.required]]
    });
  }

  changePasword(){
    $('#loading').show();

    let user = this.passwordForm.value;
    
    this.api.ChangePassword(user).subscribe(result => {
      let respuesta = result['data'];
      if (respuesta.response == 1) {
        Swal.fire({
          icon: 'success',
          title: 'Your password has been changed',
          showConfirmButton: false,
          timer: 1500
        });
      }else{
        this.error = true;

        Swal.fire({
          icon: 'error',
          title: 'Password invalid',
          showConfirmButton: false,
          timer: 1500
        });
      }
      $('#loading').hide();
    }, err => {
      $('#loading').hide();
      this.error = true;

        Swal.fire({
          icon: 'error',
          title: 'Password invalid',
          showConfirmButton: false,
          timer: 1500
        });
    });
  }

  changeTab(id){
    this.tab = id;
  }

  llenarCampos(){
    this.userForm.controls.NOMBRE.setValue(this.menu.usuario.NOMBRE);
    this.userForm.controls.TELEFONO.setValue(this.menu.usuario.TELEFONO);
    this.userForm.controls.EMAIL.setValue(this.menu.usuario.CORREO);
    this.userForm.controls.CIUDAD.setValue(this.menu.usuario.CIUDAD);
    this.userForm.controls.PAIS.setValue(this.menu.usuario.ID_PAIS);

    this.passwordForm.controls.EMAIL.setValue(this.menu.usuario.CORREO);
  }

  updateUser(){
    $('#loading').show();
    let data = this.userForm.value;

    data.ID_USER = this.menu.usuario.ID_CLIENTE;

    this.api.updateUser(data).subscribe(result => {
      let resultado = result['data'];

      if (resultado.response == 1) {
        localStorage.removeItem('user');
        localStorage.setItem('user', JSON.stringify(resultado.data));
        this.menu.getUser();

        Swal.fire({
          icon: 'success',
          title: 'Your profile has been updated',
          showConfirmButton: false,
          timer: 1500
        });

      }
      
      $('#loading').hide();
    }, err =>{
      $('#loading').hide();
    });

  }
}
