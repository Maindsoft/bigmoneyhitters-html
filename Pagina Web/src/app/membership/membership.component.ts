import * as moment from 'moment';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { MenuService } from '../services/menu.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

declare var paypal:any;
declare var $:any;

@Component({
  selector: 'app-membership',
  templateUrl: './membership.component.html',
  styleUrls: ['./membership.component.css']
})
export class MembershipComponent implements OnInit {

  @ViewChild('paypal', {static: true}) paypalElement: ElementRef;

  membresias: any;
  membresia_seleccionada: any;
  membresia_seleccionada_costo: any;
  usuario: any;
  fecha_fin: any;

  constructor(private api: ApiService, private menu: MenuService, private router: Router) { }

  ngOnInit(): void {
    this.usuario = this.menu.usuario;
    this.fecha_fin = moment().add(this.menu.usuario.VENCIMIENTO, 'days').format('yyyy/MM/DD'); 
    this.initMembreship();
  }

  initMembreship(){
    this.api.getMembership().subscribe(result => {
      let res = result['data'];
      if (res.response == 1) {
        this.membresias = res.data;
      }
    });
  }

  SelectMembership(membresia){
    this.membresia_seleccionada = membresia;
    this.membresia_seleccionada_costo = membresia.PRECIO;
    this.paypalElement.nativeElement.innerHTML = "";

    setTimeout(() => {
      this.initPaypal();
    }, 500);
  }

  initPaypal(){
    paypal.Buttons({
      createOrder: (data, actions) => {
        return actions.order.create({
          purchase_units: [
            {
              description: 'Membership by ' + this.membresia_seleccionada.NOMBRE_MEMBRESIA_INGLES,
              amount: {
                currency_code: 'USD',
                value: this.membresia_seleccionada_costo
              }
            }
          ]
        });
      },
      onApprove: async (data, actions) => {
        $('#loading').show();
        const order = await actions.order.capture();
        this.completePay(order.id);

      }
    }).render(this.paypalElement.nativeElement);
  }

  completePay(id_pago){
    this.api.insertMembership({
      ID_CLIENTE: this.usuario.ID_CLIENTE,
      ID_MEMBRESIA: this.membresia_seleccionada.ID_MEMBRESIA,
      ID_PAGO: id_pago,
      METODO_PAGO: 1
    }).subscribe(result => {
      if (result['data'].response == 1) {
        this.usuario.VENCIMIENTO = result['data'].data[0].MEMBRESIA;
        
        localStorage.removeItem('user');
        localStorage.setItem('user', JSON.stringify(this.usuario));
        
        setTimeout(() => {
          this.menu.getUser();
          this.router.navigate(['/picks']);
        }, 500);
        
      }
    });
  }
}
