import Swal from 'sweetalert2'
import * as CryptoJS from 'crypto-js';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { MenuService } from '../services/menu.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

declare var $:any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  paises: any;

  constructor(public formBuilder: FormBuilder, private api: ApiService, private menu: MenuService, private location: Location, private router: Router) {
    this.registerForm = this.createMyForm();
  }

  ngOnInit(): void {
    $('#loading').show();
    this.menu.pagina = this.location.path();

    this.getCountry();
  }

  private createMyForm(){
    return this.formBuilder.group({
      NOMBRE: ['', [Validators.required]],
      TELEFONO: ['', [Validators.required, Validators.maxLength(10)]],
      EMAIL: ['', [Validators.required, Validators.pattern('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$')]],
      PASSWORD: ['', [Validators.required, Validators.minLength(6)]],
      CIUDAD: ['', [Validators.required]],
      PAIS: ['', [Validators.required]],
      TERMINOS: [null, [Validators.required]]
    });
  }

  terminosChange(ev){
    if (ev.currentTarget.checked) {
      this.registerForm.controls.TERMINOS.setValue(1);
    } else {
      this.registerForm.controls.TERMINOS.setValue(null);
    }
  }

  getCountry(){
    this.api.getCountry().subscribe(result => {
      let resultado = result['data'];
      
      if (resultado.response == 1) {
        this.paises = resultado.data;
      }
      
      $('#loading').hide();
    }, err => {
      $('#loading').hide();
    });
  }

  saveData(){
    let data = this.registerForm.value;

    data.PASSWORD = CryptoJS.AES.encrypt(data.PASSWORD.trim(), this.api.cryptoPassword).toString();

    this.api.register(data).subscribe(result => {
      let resultado = result['data'];
      
      if (resultado.response == 1 && resultado.data[0].CLIENTE != 'El usuario ya existe') {
        localStorage.setItem('user', JSON.stringify(resultado.data[0]));
        setTimeout(() => {
          this.menu.getUser();
          this.router.navigate(['/picks']);
        }, 30);
      } else{
        Swal.fire({
          icon: 'error',
          title: 'The user is exist',
          showConfirmButton: false,
          timer: 1500
        });
      }

      $('#loading').hide();
    }, err => {
      $('#loading').hide();
    });
  }

}
