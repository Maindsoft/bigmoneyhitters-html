import * as CryptoJS from 'crypto-js';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { MenuService } from '../services/menu.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

declare var $:any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  error: boolean = false;

  constructor(public formBuilder: FormBuilder, private menu: MenuService, private location: Location, private api: ApiService, private router: Router) { 
  }

  ngOnInit(): void {
    this.menu.pagina = this.location.path();
    this.loginForm = this.createMyForm();
  }

  private createMyForm(){
    return this.formBuilder.group({
      EMAIL: ['', [Validators.required, Validators.pattern('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$')]],
      CONTRASENA: ['', [Validators.minLength(6), Validators.required]],
    });
  }


  saveData(){
    let data = this.loginForm.value;

    $('#loading').show();

    data.CONTRASENA = CryptoJS.AES.encrypt(data.CONTRASENA.trim(), this.api.cryptoPassword).toString();

    this.api.login(data).subscribe(res => {
      let respuesta = res['data'];
      if (respuesta.response == 1 && respuesta.data) {
        this.error = false;
        localStorage.setItem('user', JSON.stringify(respuesta.data));
        setTimeout(() => {
          this.menu.getUser();
          this.router.navigate(['/picks']);
        }, 30);
      }else{
        this.error = true;
        this.loginForm.reset();
      }
      $('#loading').hide();
    }, err => {
      this.error = true;
      this.loginForm.reset();
      $('#loading').hide();
    });
    
  }

}
