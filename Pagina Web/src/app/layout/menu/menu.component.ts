import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../services/menu.service';

declare var $:any;

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(public menu: MenuService, private router: Router) {
  }

  ngOnInit(): void {
  }

  scrollTo(section){
    $("html, body").animate({
      scrollTop: $(section).offset().top
    });
  }


  scrollTop(){
    let scrollToTop = window.setInterval(() => {
      let pos = window.pageYOffset;
      if (pos > 0) {
          window.scrollTo(0, pos - 20); // how far to scroll on each step
      } else {
          window.clearInterval(scrollToTop);
      }
    }, 1);
  }

  logout(){
    localStorage.removeItem('user');
    setTimeout(() => {
      this.menu.getUser();
      this.router.navigate(['/']);
    }, 30);
  }
}
