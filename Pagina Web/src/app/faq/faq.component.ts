import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MenuService } from '../services/menu.service';

declare var $:any;

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit {

  constructor(private menu: MenuService, private location: Location,) { }

  ngOnInit(): void {
    this.menu.pagina = this.location.path();
  }

  openAccordion(id_acordion){
    var body = '#accordion-body-'+id_acordion;
    var link = '#link-'+id_acordion;

    if ($(body).hasClass('show')) {
      $(body).slideUp();
      $(body).removeClass('show');
      $(link).attr('aria-expanded', false);
    }else{
      $('.body-collapse').slideUp();
      $('.body-collapse').removeClass('show');
      $('.accordion-link').attr('aria-expanded', false);

      setTimeout(() => {
        $(body).slideDown();
        $(body).addClass('show');
        $(link).attr('aria-expanded', true);
      }, 30);
    }
  }

}
