import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  pagina: string = '';
  usuario: any;

  constructor() { 
    this.getUser();
  }

  getUser(){
    this.usuario = localStorage.getItem('user');
    if (this.usuario) {
      this.usuario = JSON.parse(this.usuario);
    }
  }
}
