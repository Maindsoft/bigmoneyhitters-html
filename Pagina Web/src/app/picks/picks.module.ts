import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PicksRoutingModule } from './picks-routing.module';
import { PicksComponent } from './picks.component';


@NgModule({
  declarations: [PicksComponent],
  imports: [
    CommonModule,
    PicksRoutingModule
  ]
})
export class PicksModule { }
