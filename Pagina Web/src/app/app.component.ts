import AOS from 'aos';
import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { ApiService } from './services/api.service';
import { MenuService } from './services/menu.service';

declare var $:any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'BigMoneyWeb';

  constructor(private api: ApiService, private menu: MenuService, private router: Router){}

  ngOnInit(): void {
    this.initTemplate();
  }

  initTemplate(){

    let user = localStorage.getItem('user');
    if (user) {
      user = JSON.parse(user);
      this.getMembershipUser(user)
    }

    // initialization of go to
    $.HSCore.components.HSGoTo.init('.js-go-to');

    // initialization of HSDropdown component
    $.HSCore.components.HSDropdown.init($('[data-dropdown-target]'), {
        afterOpen: function() {
            $(this).find('input[type="search"]').focus();
        }
    });

    // initialization of masonry
    $('.masonry-grid').imagesLoaded().then(function() {
        $('.masonry-grid').masonry({
            columnWidth: '.masonry-grid-sizer',
            itemSelector: '.masonry-grid-item',
            percentPosition: true
        });
    });

    // initialization of popups
    $.HSCore.components.HSPopup.init('.js-fancybox');

    // initialization of header
    $.HSCore.components.HSHeaderSide.init($('#js-header'));
    $.HSCore.helpers.HSHamburgers.init('.hamburger');

    // initialization of HSMegaMenu component
    $('.js-mega-menu').HSMegaMenu({
        event: 'hover',
        direction: 'vertical',
        breakpoint: 991
    });
    
    AOS.init({
      // Global settings:
      disable: 'mobile', // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
      startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
      initClassName: 'aos-init', // class applied after initialization
      animatedClassName: 'aos-animate', // class applied on animation
      useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
      disableMutationObserver: false, // disables automatic mutations' detections (advanced)
      debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
      throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)

      // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
      offset: 120, // offset (in px) from the original trigger point
      delay: 50, // values from 0 to 3000, with step 50ms
      duration: 1200, // values from 0 to 3000, with step 50ms
      easing: 'ease', // default easing for AOS animations
      once: false, // whether animation should happen only once - while scrolling down
      mirror: false, // whether elements should animate out while scrolling past them
      anchorPlacement: 'top-bottom', // defines which position of the element regarding to window should trigger the animation

    });
  }

  onActivate(event) {
    let scrollToTop = window.setInterval(() => {
        let pos = window.pageYOffset;
        if (pos > 0) {
            window.scrollTo(0, pos - 20); // how far to scroll on each step
        } else {
            window.clearInterval(scrollToTop);
        }
    }, 16);
  }

  getMembershipUser(user){
    this.api.getUserMembership({ID: user.ID_CLIENTE}).subscribe(res => {
      let respuesta = res['data'];
      if (respuesta.response == 1) {
        user.VENCIMIENTO = respuesta.data.MEMBRESIA;
        
        localStorage.removeItem('user');
        localStorage.setItem('user', JSON.stringify(user));
        
        setTimeout(() => {
          this.menu.getUser();

          if (this.menu.usuario.VENCIMIENTO < 0) {
            this.router.navigate(['/membership']);
          }
        }, 500);
      }
    });
  }

}
