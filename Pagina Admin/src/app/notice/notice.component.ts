import * as moment from 'moment';
import Swal from 'sweetalert2';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-notice',
  templateUrl: './notice.component.html',
  styleUrls: ['./notice.component.css']
})
export class NoticeComponent implements OnDestroy, OnInit {

  news: any;
  dtOptions: DataTables.Settings = {};
  load: boolean = true;

  dtTrigger: any = new Subject();

  constructor(private api: ApiService,
              private router: Router) {
  }

  ngOnInit(): void {

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };

    this.initNews();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  initNews(){
    this.load = true;

    this.api.getNotice().subscribe(res => {
      this.news = res['data'].data;
      this.load = false;
    })
    this.dtTrigger.next();
  }

  refresh(){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['/news']);
    });
  }

  editNew(id){

  }

  deleteNew(id){
    this.api.DeleteNews({id: id}).subscribe(res => {
      
    });
  }
}
