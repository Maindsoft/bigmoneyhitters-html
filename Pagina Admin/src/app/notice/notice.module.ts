import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NoticeRoutingModule } from './notice-routing.module';
import { NoticeComponent } from './notice.component';

import { DataTablesModule } from 'angular-datatables';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { CreateNewsComponent } from './create-news/create-news.component';

@NgModule({
  declarations: [NoticeComponent, CreateNewsComponent],
  imports: [
    FormsModule,
    CommonModule,
    DataTablesModule,
    AngularEditorModule,
    NoticeRoutingModule,
    ReactiveFormsModule
  ]
})
export class NoticeModule { }
