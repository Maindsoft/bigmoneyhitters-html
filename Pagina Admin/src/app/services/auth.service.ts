import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public user: any;

  constructor() {
    this.getUser();
  }

  getUser(){
    this.user = localStorage.getItem('user');
    if (this.user) {
      this.user = JSON.parse(this.user);
    }
  }

  login(user){
    localStorage.setItem('user', JSON.stringify(user));
    this.getUser();
  }

  logout(){
    localStorage.removeItem('user');
    this.getUser();
  }
}
