import Swal from 'sweetalert2';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnDestroy, OnInit {

  @ViewChild('myModal') myModal;
  private modalRef;

  userForm: FormGroup;
  users: any;
  dtOptions: DataTables.Settings = {};
  load: boolean = true;

  dtTrigger: any = new Subject();
  
  closeResult: string;

  membresias: any;
  metodos_pago: any;

  constructor(private api: ApiService, private router: Router, private modalService: NgbModal, public formBuilder: FormBuilder,) { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      order: [[ 0, "desc" ]]
    };

    this.initUser()
    this.getMemberships();
    this.userForm = this.createNewUser();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  private createNewUser(){
    return this.formBuilder.group({
      ID_CLIENTE: ['', [Validators.required]],
      ID_SUSCRIPTION: ['', [Validators.required]],
      PAYMENT: ['', [Validators.required]]
    });
  }

  initUser(){
    this.load = true;

    this.api.getUsers().subscribe(res => {
      let result = res['data'];
      if (result.response == 1) {
        this.users = result.data;
      }
      this.dtTrigger.next();

      this.load = false;
    }, err => {
      this.load = false;
    });
  }

  editUser(id){
    this.router.navigate(['/users/update-user', id]);
  }

  historyUser(id){
    this.router.navigate(['/users/history', id]);
  }

  deleteUser(id){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.confirmDelete(id);
      }
    })
  }

  confirmDelete(id){
    this.load = true;
    this.api.deleteUsers({ id: id }).subscribe(res => {
      let resp = res['data'];
      if(resp.response == 1){
        this.refresh();
      }
      this.load = false;
    });
  }

  deletedUsers(){
    this.router.navigate(['/users/deleted-users']);
  }

  refresh(){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['/users']);
    });
  }

  getMemberships(){
    this.api.getMembresias().subscribe(res => {
      let resultado = res['data'];
      
      if (resultado.response == 1) {
        this.membresias = resultado.data;
      }

      this.getPaymenths();
    });
  }

  getPaymenths(){
    this.api.getMetodosPagos().subscribe(res => {
      let resultado = res['data'];
      
      if (resultado.response == 1) {
        this.metodos_pago = resultado.data;
      }
    });
  }
  
  insertMembership(id, content){
    this.userForm.controls.ID_CLIENTE.setValue(id);
    this.modalRef = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'});
  }

  saveData(){
    let data = this.userForm.value;
    
    this.api.insertMembership({
      ID_CLIENTE: data.ID_CLIENTE,
      ID_MEMBRESIA: data.ID_SUSCRIPTION,
      ID_PAGO: 1,
      METODO_PAGO: data.PAYMENT
    }).subscribe(result => {
      if (result['data'].response == 1) {
        this.modalService.dismissAll();
        this.refresh();
      }
    });

  }

}
