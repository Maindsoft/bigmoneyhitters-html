import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserComponent } from './user.component';
import { UserRoutingModule } from './user-routing.module';
import { NewUserComponent } from './new-user/new-user.component';

import { DataTablesModule } from 'angular-datatables';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaymentsComponent } from './payments/payments.component';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { DeletedComponent } from './deleted/deleted.component';

@NgModule({
  declarations: [UserComponent, NewUserComponent, PaymentsComponent, DeletedComponent],
  imports: [
    NgbModule,
    FormsModule,
    CommonModule,
    DataTablesModule,
    UserRoutingModule,
    ReactiveFormsModule
  ]
})
export class UserModule { }
