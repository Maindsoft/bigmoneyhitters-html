import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-deleted',
  templateUrl: './deleted.component.html',
  styleUrls: ['./deleted.component.css']
})
export class DeletedComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  dtTrigger: any = new Subject();
  load: boolean = true;
  users: any;

  constructor(private api: ApiService, private router: Router) { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      order: [[ 0, "desc" ]]
    };

    this.initUser()
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  refresh(){
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['/users/deleted-users']);
    });
  }

  initUser(){
    this.load = true;

    this.api.getDeletedUsers().subscribe(res => {
      let result = res['data'];
      if (result.response == 1) {
        this.users = result.data;
      }
      this.dtTrigger.next();

      this.load = false;
    }, err => {
      this.load = false;
    });
  }
}
