import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserComponent } from './user.component';
import { DeletedComponent } from './deleted/deleted.component';
import { NewUserComponent } from './new-user/new-user.component';
import { PaymentsComponent } from './payments/payments.component';

const routes: Routes = [
  { path: '', component: UserComponent },
  { path: 'deleted-users', component: DeletedComponent },
  { path: 'new-user', component: NewUserComponent },
  { path: 'update-user/:id', component: NewUserComponent },
  { path: 'history', component: PaymentsComponent },
  { path: 'history/:id', component: PaymentsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
