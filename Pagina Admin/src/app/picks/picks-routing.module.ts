import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PicksComponent } from './picks.component';
import { NewPicksComponent } from './new-picks/new-picks.component';

const routes: Routes = [
  { path: '', component: PicksComponent },
  { path: 'new-picks', component: NewPicksComponent },
  { path: 'update-picks/:id_picks', component: NewPicksComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PicksRoutingModule { }
