import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPicksComponent } from './new-picks.component';

describe('NewPicksComponent', () => {
  let component: NewPicksComponent;
  let fixture: ComponentFixture<NewPicksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPicksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPicksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
