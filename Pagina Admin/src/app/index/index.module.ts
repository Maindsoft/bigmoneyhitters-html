import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IndexRoutingModule } from './index-routing.module';
import { IndexComponent } from './index.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [IndexComponent],
  imports: [
    FormsModule,
    CommonModule,
    IndexRoutingModule
  ]
})
export class IndexModule { }
